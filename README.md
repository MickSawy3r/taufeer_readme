# Taufeer Client API Contracts



This Document is used to determine the user API contracts or Taufeer Server API.



## Application SRS



1. Firebase Notifications (Firebase cloud Messaging.



## Areas List

i.e. Upper Saudi, Lower Saudi

#### Request: 

```php
GET http://madaaaltawfir.com//api/area/
```

#### Response

```json
[
    {
        "id": 1,
        "name": "London",
        "image": "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d7/United_Kingdom_relief_location_map.jpg/800px-United_Kingdom_relief_location_map.jpg",
        "createdAt": "2019-12-23T11:47:58.000Z",
        "updatedAt": "2019-12-23T11:47:58.000Z"
    },
    {
        "id": 2,
        "name": "paris",
        "image": "https://server.com/1.jpg",
        "createdAt": "2019-12-23T11:50:14.000Z",
        "updatedAt": "2019-12-23T11:50:14.000Z"
    }
]
```



## Cities List

#### Request

```
GET http://madaaaltawfir.com/api/city/<area_id>
```

Note: Area ID is available as id in the areas list.

#### Response

```json
[
    {
        "id": 1,
        "name": "london - city 01",
        "areaId": 1,
        "image": "https://server.com/1.jpg",
        "createdAt": "2019-12-23T11:52:17.000Z",
        "updatedAt": "2019-12-23T11:52:17.000Z"
    },
    {
        "id": 2,
        "name": "london - city 02",
        "areaId": 1,
        "image": "https://server.com/1.jpg",
        "createdAt": "2019-12-23T11:52:17.000Z",
        "updatedAt": "2019-12-23T11:52:17.000Z"
    }
]
```

## Ads List

#### Request

```
http://madaaaltawfir.com/api/ad/<network_name>/<city_id>
```

Network_name:

1. commercial
2. educational
3. medical
4. entertainment



#### Response

```json
[
    {
        "client": "Burger King",
        "title": "Discounts 03!!",
        "logo": "https://logomaster.ai/static/media/gallery002.936afb9d.png"
    },
    {
        "client": "Burger King",
        "title": "Discounts 02!!",
        "logo": "https://logomaster.ai/static/media/gallery002.936afb9d.png"
    },
    {
        "client": "Burger King",
        "title": "Discounts 01!!",
        "logo": "https://logomaster.ai/static/media/gallery002.936afb9d.png"
    }
]
```



## Ad Details

#### Request

```
http://madaaaltawfir.com/api/ad/<Ad_Id>
```

#### Response

```json
{
    "id": 1,
    "title": "Discounts 01!!",
    "network": "commercial",
    "clientId": 1,
    "cityId": 1,
    "media": "https://logomaster.ai/static/media/gallery002.936afb9d.png",
    "body": "New Discounts on our new product",
    "comment": "Only 14 Left!",
    "active": true,
    "createdAt": "2019-12-23T12:08:29.000Z",
    "updatedAt": "2019-12-23T12:08:29.000Z",
    "client": {
        "id": 1,
        "name": "Burger King",
        "active": true,
        "facebook": "somelink",
        "twitter": "somelink",
        "linkedin": "somelink",
        "snapchat": "somelink",
        "instagram": "somelink",
        "telegram": "somelink",
        "phone01": "0953691509",
        "phone02": "somelink",
        "phone03": "somelink",
        "website": "somelink",
        "logo": "https://logomaster.ai/static/media/gallery002.936afb9d.png",
        "location": "geo:37.7749,-122.4194",
        "createdAt": "2019-12-23T12:05:02.000Z",
        "updatedAt": "2019-12-23T12:05:02.000Z"
    }
}
```



### Special Ads

#### Request

```
GET http://madaaaltawfir.com/api/ad/special/
```

#### Response

```json
[
    {
        "client": "Burger King",
        "title": "Discounts 03!!",
        "logo": "https://logomaster.ai/static/media/gallery002.936afb9d.png",
        "id": 1
    },
    {
        "client": "Burger King",
        "title": "Discounts 02!!",
        "logo": "https://logomaster.ai/static/media/gallery002.936afb9d.png",
        "id": 2
    },
    {
        "client": "Burger King",
        "title": "Discounts 01!!",
        "logo": "https://logomaster.ai/static/media/gallery002.936afb9d.png",
        "id": 3
    }
]
```



## Search



#### Request

```php
GET http://madaaaltawfir.com/api/search/<search_query>
```



#### Response

```json
[
    {
        "client": "Burger King",
        "title": "Discounts 03!!",
        "logo": "https://logomaster.ai/static/media/gallery002.936afb9d.png",
        "id": 1
    },
    {
        "client": "Burger King",
        "title": "Discounts 02!!",
        "logo": "https://logomaster.ai/static/media/gallery002.936afb9d.png",
        "id": 2
    },
    {
        "client": "Burger King",
        "title": "Discounts 01!!",
        "logo": "https://logomaster.ai/static/media/gallery002.936afb9d.png",
        "id": 3
    }
]
```





## Ads List Header



#### Request

```
GET http://madaaaltawfir.com/api/header
```



#### Response

```json
{
	"commercial": "http://some.com/pic.jpg",
    "medical": "http://some.com/pic.jpg",
    "educational": "http://some.com/pic.mp4",
    "entertainment": "http://some.com/pic.mp4"
}
```

note: ext is either an image or a mp4 video.



##### Best Regards: 6Bits



